(function () {
    'use strict';
    angular.module('app').config(function ($mdThemingProvider, $mdIconProvider) {

        $mdIconProvider
            .defaultIconSet("./assets/svg/avatars.svg", 128)
            .icon("menu", "./assets/svg/menu.svg", 24)
            .icon("share", "./assets/svg/share.svg", 24)
            .icon("google_plus", "./assets/svg/google_plus.svg", 512)
            .icon("hangouts", "./assets/svg/hangouts.svg", 512)
            .icon("twitter", "./assets/svg/twitter.svg", 512)
            .icon("phone", "./assets/svg/phone.svg", 512);

        $mdThemingProvider.theme('default')
            .primaryPalette('indigo')
            .accentPalette('orange');
        $mdThemingProvider.theme('altTheme')
            .primaryPalette('blue');
    })
        .controller('contatoController', ['contatoService', '$q', '$mdDialog', '$mdToast', ContatoController]);



    function ContatoController(contatoService, $q, $mdDialog, $mdToast) {
        var self = this;

        self.selected = {};
        self.contatos = [];
        self.selectedIndex = 0;
        self.filterText = null;
        self.oldContato = null;
        self.editando = false;
        self.alterado = false;
        self.doBanco = false;
        self.selectContato = selectContato;
        self.deleteContato = deleteContato;
        self.saveContato = saveContato;
        self.createContato = createContato;
        self.filter = filterContato;
        self.editarContato = editarContato;
        self.cancelarEdicao = cancelarEdicao;
        self.adicionarTelefone = adicionarTelefone;
        self.adicionarEndereco = adicionarEndereco;
        self.tiposTelefone = [
            "Telefone",
            "Celular",
            "Fax",
            "Outro"
        ];

        self.tiposEndereco = [
            "Casa",
            "Trabalho",
            "Outro"
        ]

        Notification.requestPermission(function (val) { console.log(val); });
        // --> nothing happens
        console.log(Notification.permission);
        // --> "granted"
        new Notification("Hello Atom");
        // --> nothing happens
        
        var notifier = require('nw-notify');
        notifier.setTemplatePath('node_modules/nw-notify/notification.html');
        notifier.notify("Teste", "1234");

        // Load initial data
        getAllContatos();
        
        //----------------------
        // Internal functions 
        //----------------------
        
        function editarContato() {
            self.oldContato = clone(self.selected);
            self.editando = true;
            self.alterado = false;
        }

        function cancelarEdicao() {
            self.editando = false;
            self.alterado = false;
            self.selected = clone(self.oldContato);
        }

        function selectContato(contato, index) {
            self.selected = angular.isNumber(contato) ? self.contatos[contato] : contato;
            self.selectedIndex = angular.isNumber(contato) ? contato : index;
        }

        function deleteContato($event) {
            var confirm = $mdDialog.confirm()
                .title('Remover contato?')
                .content('Quer mesmo remover este contato?')
                .ok('Sim')
                .cancel('Não')
                .targetEvent($event);


            $mdDialog.show(confirm).then(function () {
                contatoService.destroy(self.selected.contato_id).then(function (affectedRows) {
                    self.contatos.splice(self.selectedIndex, 1);
                });
            }, function () { });
        }

        function saveContato($event) {
            if (self.selected != null && self.selected._id != null) {
                contatoService.update(self.selected).then(function (affectedRows) {
                    getAllContatos();
                    self.editando = false;
                    self.alterado = false;
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Contato editado!')
                            .position('top right')
                            .hideDelay(2000)
                        );
                });
            }
            else {
                //self.selected.contato_id = new Date().getSeconds();
                contatoService.create(self.selected).then(function (affectedRows) {
                    getAllContatos();
                    self.editando = false;
                    self.alterado = false;
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Contato salvo!')
                            .position('top right')
                            .hideDelay(2000)
                        );
                });
            }
        }

        function createContato() {
            self.selected = {};
            self.editando = true;
            self.alterado = false;
            self.selectedIndex = null;
        }

        function getAllContatos() {
            contatoService.getContatos().then(function (contatos) {
                self.contatos = [].concat(contatos);
                if (contatos.length > 0) {
                    self.selected = contatos[0];
                }
            });
        }

        function filterContato() {
            if (self.filterText == null || self.filterText == "") {
                getAllContatos();
            }
            else {
                contatoService.getByName(self.filterText).then(function (contatos) {
                    self.contatos = [].concat(contatos);
                    //self.selected = contatos[0];
                });
            }
        }

        function adicionarEndereco() {
            if (!self.selected.enderecos)
                self.selected.enderecos = new Array();
            self.selected.enderecos.push({ tipo: "", endereco: "" });
        }

        function adicionarTelefone() {
            if (!self.selected.telefones)
                self.selected.telefones = new Array();
            self.selected.telefones.push({ tipo: "", numero: "" });
        }

        function clone(src) {
            return JSON.parse(JSON.stringify(src));
        }
    }

})();