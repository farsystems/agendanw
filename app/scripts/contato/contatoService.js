(function () {
    'use strict';
    var mysql = require('mysql');

    var tungus = require('tungus');
    var mongoose = require('mongoose')
    var Schema = mongoose.Schema;

    var contatoSchema = Schema({
        nome: String,
        sobrenome: String,
        email: String,
        enderecos: [
            {
                tipo: String,
                endereco: String
            }
        ],
        telefones: [
            {
                tipo: String,
                numero: String
            }
        ]
    })
    var Contato = mongoose.model('Contato', contatoSchema);

    mongoose.connect('tingodb://' + process.cwd() + '/data', function (err) {
        // if we failed to connect, abort
        if (err) throw err;
    });
    
    // Creates MySql database connection
    var connection = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "password",
        database: "contato_manager"
    });

    angular.module('app')
        .service('contatoService', ['$q', ContatoService]);

    function ContatoService($q) {
        return {
            getContatos: getContatos,
            getById: getContatoById,
            getByName: getContatoByName,
            create: createContato,
            destroy: deleteContato,
            update: updateContato
        };

        function getContatos() {
            var deferred = $q.defer();
            Contato.find({}, function (err, rows) {
                if (err) deferred.reject(err);
                deferred.resolve(rows);
            });

            return deferred.promise;
        }

        function getContatoById(id) {
            var deferred = $q.defer();
            var query = "SELECT * FROM contatos WHERE contato_id = ?";
            connection.query(query, [id], function (err, rows) {
                if (err) deferred.reject(err);
                deferred.resolve(rows);
            });
            return deferred.promise;
        }

        function getContatoByName(name) {
            var deferred = $q.defer();
            var query = "SELECT * FROM contatos WHERE name LIKE  '" + name + "%'";
            connection.query(query, [name], function (err, rows) {
                if (err) deferred.reject(err);

                deferred.resolve(rows);
            });
            return deferred.promise;
        }

        function createContato(contato) {
            var deferred = $q.defer();
            Contato.create({
                nome: contato.nome,
                sobrenome: contato.sobrenome,
                email: contato.email,
                enderecos : contato.enderecos,
                telefones: contato.telefones
            }, function (err) {
                if (err) deferred.reject(err);
                deferred.resolve();

            });
            return deferred.promise;
        }

        function deleteContato(id) {
            var deferred = $q.defer();
            var query = "DELETE FROM contatos WHERE contato_id = ?";
            connection.query(query, [id], function (err, res) {
                if (err) deferred.reject(err);
                deferred.resolve(res.affectedRows);
            });
            return deferred.promise;
        }

        function updateContato(contato) {
            var deferred = $q.defer();
            var query = "UPDATE contatos SET name = ? WHERE contato_id = ?";
            connection.query(query, [contato.name, contato.contato_id], function (err, res) {
                if (err) deferred.reject(err);
                deferred.resolve(res);
            });
            return deferred.promise;
        }
    }
})();